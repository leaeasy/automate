#!/usr/bin/env python

import logging
import os

class Log():
    _instance = None
    def __init__(self, logfile):
        if not os.path.exists(os.path.dirname(logfile)):
            os.makedirs(os.path.dirname(logfile))
        logging.basicConfig(level = logging.DEBUG,
                format='%(asctime)s %(levelname)s %(message)s',
                filename=logfile,
                datefmt="%Y-%m-%d %H:%M:%S",
                filemode='a')

    @classmethod
    def info(self, str):
        logging.info(str)

    @classmethod
    def warn(self, str):
        logging.warning(str)

    @classmethod
    def error(self, str):
        logging.error(str)
