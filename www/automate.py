#! /usr/bin/env python
# -*- coding: utf-8 -*-
#

from flask import render_template, Flask, url_for, request, jsonify
import flask
import functions
import json
import config
import os
import glob
from auth import Authentication
from auth import AAARedirect

app = Flask(__name__)
app.secret_key = 'automate'
aaa = Authentication('save')
instances = functions.get_instances()

def post_get(name, default=""):
    v = request.form.get(name, default).strip()
    return str(v)

def get_user(default="anoymous"):
    try:
        user = aaa.current_user
    except:
        return default
    return user.username

@app.errorhandler(AAARedirect)
def redirect_exception_handler(e):
    return flask.redirect(e.message)


@app.route("/login", methods=["POST"])
def login():
    username = post_get("username")
    password = post_get("password")
    aaa.login(username, password, success_redirect="/", fail_redirect="/login")

@app.route("/login")
def login_form():
    return render_template("login_form.html")

@app.route("/logout")
def logout():
    aaa.logout(success_redirect="/")

@app.route("/")
@app.route("/instance/<instance>/build")
@app.route("/instance/<instance>")
def index(instance=None):
    builds = []
    if instance is None:
        instance = instances[0]
    for build in functions.get_builds(instance):
        builds.append(functions.get_build(instance, build))
    return render_template("builds.html", instances=instances, instance=instance, builds=builds, user=get_user())

@app.route("/instance/<instance>/build/<build_id>")
def build(instance, build_id):
    build = functions.get_build(instance, build_id)
    return render_template("build.html", instance=instance, build=build, user=get_user())

@app.route("/instance/<instance>/incoming")
def incoming(instance):
    incomings = functions.get_incomings(instance)
    return render_template("incomings.html", incomings=incomings, user=get_user(), instance=instance,instances=instances)

@app.route("/instance/<instance>/repository")
def repository(instance):
    repos = functions.get_repositories(instance)
    repos_info = {}
    for repo in repos:
        repos_info[repo] = functions.get_repository(instance, repo) 
    return render_template("repository.html", repos=repos_info, user=get_user(), instance=instance, instances=instances)

@app.route("/instance/<instance>/rebuild")
def rebuild(instance):
    aaa.require(role="admin", fail_redirect="/login")
    build_id = request.args.get("build_id")
    dist = request.args.get("dist")
    arch = request.args.get("arch")

    build_file = os.path.join(config.build_path(instance), build_id, "build.json")
    if os.path.exists(build_file):
        build = json.load(open(build_file, "r"))
    else:
        raise OSError("Missing %s" % build_file)

    ret_file = os.path.join(config.build_path(instance), build_id, "log", "%s_%s.ret" % (dist, arch))
    queue = {}
    queue['build_id'] = build_id
    queue['arch'] = arch
    queue['dist'] = dist

    queue['package'] = build['package']
    queue['version'] = build['version']
    queue['maintainer'] = build['maintainer']
    queue['changed_by'] = build['changed_by']
    queue['build_dir'] = build['build_dir']

    queue_filename = "%(build_id)s_%(package)s_%(version)s_%(dist)s_%(arch)s.json" % (queue)

    queue_file = os.path.join(config.queue_path(instance), queue_filename)
    with open(queue_file, 'w') as fp:
        fp.write(json.dumps(queue, indent=4))
    return jsonify(queue)

@app.route("/instance/log")
def log():
    instance = request.args.get('instance')
    build_id = request.args.get('build_id')
    dist = request.args.get('dist')
    arch = request.args.get('arch')
    build_file = os.path.join(config.build_path(instance), build_id, "build.json")
    if os.path.exists(build_file):
        build = json.load(open(build_file, "r"))
        build['arch'] = arch
        build['dist'] = dist
    log_file = os.path.join(config.build_path(instance), build_id, "log", "%s_%s.log" % (dist, arch))
    ret_file = os.path.join(config.build_path(instance), build_id, "log", "%s_%s.ret" % (dist, arch))
    update_file = "%s.update" % log_file
    logs = ""
    if os.path.exists(update_file):
        for line in open(update_file).readlines():
            logs += line
    if os.path.exists(log_file):
        for line in open(log_file).readlines():
            logs += line

    is_building = not(os.path.exists(ret_file))

    return render_template("log.html", user=get_user(), instance=instance, instances=instances, is_building=is_building, logs=logs ,build=build)

@app.route("/api/v1/<instance>/<build_id>", methods=['POST'])
def get_build_status(instance, build_id):
    build_folder = os.path.join(config.build_path(instance), build_id)
    build_file = os.path.join(build_folder, "build.json")
    import_log_file = os.path.join(build_folder, "import.log")
    import_request_file = os.path.join(build_folder, "import.request")
    import_ignore_file = os.path.join(build_folder, "import.ignore")
    import_error_file = os.path.join(build_folder, "import.error")

    build = json.load(open(build_file, "r"))

    result = {'status':'', 'jobs':[]}
    all_ok = True
    for dist in build['dists']:
        for arch in build['archs']:
            build_ok = True
            ret_file = os.path.join(build_folder, "log", "%s_%s.ret" % (dist, arch))
            log_file = os.path.join(build_folder, "log", "%s_%s.log" % (dist, arch))
            queue_file = glob.glob(os.path.join(config.queue_path(instance), "%s_*_%s_%s.json" % (build_id, dist, arch)))
            update_file = "%s.update" % log_file
            status = "warning"
            if len(queue_file) != 0:
                status = 'warning'

            elif os.path.exists(ret_file):
                res = open(ret_file).readline().strip()
                if res == '0':
                    status = 'success'
                else:
                    status = 'danger'
                    build_ok = False
                    
            else:
                if not os.path.exists(log_file) and not os.path.exists(update_file):
                    status = 'warning'
                    build_ok = False
                else:
                    status = 'primary'

            all_ok = all_ok & build_ok
            result['jobs'].append({"%s-%s" % (dist, arch): status})

    if os.path.exists(import_request_file):
        result['status'] = 'import requested'
    elif os.path.exists(import_ignore_file):
        result['status' ] = 'import ignored'
    elif os.path.exists(import_log_file):
        result['status'] = 'import imported'
    elif os.path.exists(import_error_file):
        result['status'] = 'import errored'
    elif not all_ok:
        result['status'] = 'build error'
    else:
        result['status'] = 'building'

    return jsonify(result)


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True, use_reloader=True)
