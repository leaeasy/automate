#! /usr/bin/env python
# -*- coding: utf-8 -*-
#
import os

salt = '123'
automate_path = "/home/isobuilder/automate"

def build_path(instance):
    return os.path.join(automate_path, "instances", instance, "builds")

def queue_path(instance):
    return os.path.join(automate_path, "instances", instance, "queue")
