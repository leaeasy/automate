#! /usr/bin/env python
# -*- coding: utf-8 -*-
#

import config
import glob
import os
import natsort
import json
from debian import deb822
import gzip

def get_instances():
    instances = []
    for _instance in glob.glob(os.path.join(config.automate_path,"instances","*")):
        if os.path.exists(os.path.join(_instance, 'automate.conf')):
            instances.append(os.path.basename(_instance))
        instances.reverse()
    return instances

def get_builds(instance, max_builds=50):
    builds = []
    for build in glob.glob(os.path.join(config.automate_path, "instances", instance, "builds", "*")):
        if os.path.isdir(build):
            builds.append(os.path.basename(build))
    builds = sorted(builds, key = lambda x: natsort.natsort_key(x), reverse=True)
    return builds[:max_builds]

def get_incomings(instance):
    incomings = []
    for incoming in glob.glob(os.path.join(config.automate_path, "instances", instance, "upload", "*.dsc")):
        dsc_file = os.path.basename(incoming)
        with open(incoming) as fp:
            incomings.append(deb822.Dsc(fp))
    return incomings

def get_repositories(instance):
    repositories = []
    for repo in glob.glob(os.path.join(config.automate_path, "instances", instance, "repository", "*")):
        if os.path.exists(os.path.join(repo, "dists")):
            repositories.append(os.path.basename(repo))
    return repositories

def get_repository(instance, repo_name):
    packages = glob.glob(os.path.join(config.automate_path, "instances", instance, "repository", repo_name, "pool", "main", "*/*/*.deb"))
    packages_info = {}
    for package in packages:
        _package = os.path.basename(os.path.dirname(package))
        if packages_info.get(_package):
            packages_info[_package].append(os.path.basename(package))
        else:
            packages_info[_package] = [os.path.basename(package)]
    return(packages_info)

def get_build(instance, build_id):
    json_file = os.path.join(os.path.join(config.automate_path, 'instances', instance, 'builds', build_id, 'build.json'))
    build =  json.load(open(json_file, "r"))
    changes_file = "%s_%s_source.changes" % (build['package'], build['version'].split(':')[-1])
    changes = os.path.join(os.path.join(config.automate_path, 'instances', instance, 'builds', build_id, 'source', changes_file))
    with open(changes) as fp:
        deb_changes = deb822.Changes(fp)
    build['changes'] = deb_changes['Changes']
    return build
