$(document).ready(function(){
	instance_name = $("#instance_name").text();
	$(".tr-build").each(function() {
		var id=$(this).attr('id').split('-').pop();
		var that=this;
		$.post("/api/v1/"+instance_name+"/"+id, function(result){
			switch(result.status){
				case("import imported"):
					$(that).addClass('info');
					img="aptdaemon-add";
					status="Import Success";
					break;
				case("import ignored"):
					img="aptdaemon-cleanup";
					status="Ignore import";
					break;
				case("import requested"):
					img="aptdaemon-wait";
					status="Import Queue";
					break;
				case("import errored"):
					img="aptdaemon-delete";
					status="Import errored";
					break;
				case("build error"):
					img="aptdaemon-delete";
					status="Build errror";
					break;
				case("building"):
					img="aptdaemon-working";
					status="Building";
					break;
			};
			$("#build-" + id + "-img").attr("src", "/static/img/" + img + ".png");
			$("#build-" + id + "-img").attr("title", status);

			$.each(result.jobs, function(_, jobs) {
				for (var job in jobs) {
					$("#tr-build-" + id + "-" + job).addClass("label-" + jobs[job]);
				}
			});
		});
	})
})
