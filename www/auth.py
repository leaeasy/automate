#!/usr/bin/env python
import base64
import hashlib
import os
import shutil
from datetime import datetime

import json
import flask

class AAAException(Exception):
    pass

class AAAStorgeException(AAAException):
    pass

class AuthException(AAAException):
    pass

class AAARedirect(Exception):
    pass

class BytesEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, bytes):
            return obj.decode()
        return json.JSONDecoder.default(self, obj)

class JsonBackend():
    def __init__(self, directory, users_fname="users", roles_fname="roles", initialize=False):
        self._directory = directory
        self.users = {}
        self._users_fname = users_fname
        self.roles = {}
        self._roles_fname = roles_fname
        self._mtimes = {}
        if initialize:
            self._initialize_storage()
        self._refresh()

    def _initialize_storage(self):
        roles = {'admin': '1000'}
        self._savejson(self._roles_fname, roles)
        user = {'admin': {'role': 'admin', 'hash': 'FFHG+qNetow7ClpTCZcKYLiprxtYD3dDjw5PHlmXfXVns0Op3vHjnnPguq6bzpXRtdxI3X3ZLAIJ3qAyCaqu2g==', 'desc':'Admin'}}
        self._savejson(self._users_fname, user)

    def _refresh(self):
        self._loadjson(self._users_fname, self.users)
        self._loadjson(self._roles_fname, self.roles)

    def _loadjson(self, fname, dest):
        try:
            fname = "%s/%s.json" % (self._directory, fname)
            mtime = os.stat(fname).st_mtime
            if self._mtimes.get(fname, 0) == mtime:
                return

            with open(fname) as fp:
                json_data = fp.read()
        except Exception as e:
            raise AAAStorgeException("Unable to read json file %s: %s " % (fname, e))

        try:
            json_obj = json.loads(json_data)
            dest.clear()
            dest.update(json_obj)
            self._mtimes[fname] = os.stat(fname).st_mtime
        except Excetpin as e:
            raise AAAStorgeException("Unable to parse JSON data from %s: %s" % (fname, e))

    def _savejson(self, fname, obj):
        fname = "%s/%s.json" %(self._directory, fname)
        try:
            with open("%s.tmp" % fname, 'w') as fp:
                json.dump(obj, fp, cls = BytesEncoder, indent=4)
                fp.flush()
            shutil.move("%s.tmp" % fname, fname)
        except Exception as e:
            raise AAAStorgeException("Unable to save JSON file %s: %s" % (fname, e))

    def save_users(self):
        self._savejson(self._users_fname, self.users)

    def save_roles(self):
        self.savejson(self._roles_fname, self.roles)


class Authentication():
    def __init__(self, directory=None, initialize=False, session_domain=None, session_key_name=None):
        self.session_key_name = session_key_name or 'beaker.session'
        self._store = JsonBackend(directory, users_fname='users', roles_fname='roles', initialize=initialize)
        self.session_domain = session_domain
        self.session = flask.session

    def login(self, username, password, success_redirect=None, fail_redirect=None):
        if username in self._store.users:
            salted_hash = self._store.users[username]['hash']
            if hasattr(salted_hash, 'encode'):
                salted_hash = salted_hash.encode('ascii')
            authenticated = self._verify_password(username, password, salted_hash)

            if authenticated:
                self._setup_cookie(username)
                self._store.users[username]['last_login'] = str(datetime.utcnow())
                self._store.save_users()
                if success_redirect:
                    self._redirect(success_redirect)
                return True

        if fail_redirect:
            self._redirect(fail_redirect)
        return False

    def logout(self, success_redirect='/login', fail_redirect='/login'):
        try:
            self.session.pop('username')
        except Exception as e:
            self._redirect(fail_redirect)
        self._redirect(success_redirect)

    def require(self, username=None, role=None, fixed_role=False, fail_redirect=None):
        if username is not None:
            if username not in self._store.users:
                raise AAAException("User is not exists")

        if fixed_role and role is None:
            raise AAAException("A role must be specified if fixed_role is set")

        if role is not None and role not in self._store.roles:
            raise AAAException("Role is not exists")

        try:
             cu = self.current_user
        except AAAException:
            if fail_redirect is None:
                raise AuthException("Unauthenticated user")
            else:
                self._redirect(fail_redirect)

        if cu.role not in self._store.roles:
            raise AAAException("Role is not found")

        if username is not None:
            if username == self.current_user.username:
                return
            if fail_redirect is None:
                raise AAAException("Unauthenticated access: Incorrect username")
            self._redirect(fail_redirect)

        if fixed_role:
            if role == self.current_user.role:
                return
            if fail_redirect is None:
                raise AAAException("Unauthenticated access: Incorrect role")
            self._redirect(fail_redirect)

        if role is not None:
            current_lvl = self._store.roles[self.current_user.role]
            threshold_lvl = self._store.roles[role]
            if current_lvl >= threshold_lvl:
                return
            if fail_redirect is None:
                raise AAAException("Unauthenticated access: Incorrect role")
            self._redirect(fail_redirect)

        return

    def create_role(self, role, level):
        if self.current_user.level < 100:
            raise AAAException("The current user is not authorized to")
        if role in self._store.roles:
            raise AAAException("The role is already exists")

        try:
            int(level)
        except ValueError:
            raise AAAException("The level must be numeric.")

        self._store.roles[role] = level
        self._store.save_roles()

    def delete_role(self, role):
        if self.current_user.level < 100:
            raise AAAException("The current user is not authorized to")
        if role not in self._store.roles:
            raise AAAException("The role is not exists")

        self._store.roles.pop(role)
        self._store.save_roles()

    def list_roles(self):
        for role in sorted(self._store.roles):
            yield (role, self._store.roles[role])

    def create_user(self, username, role, password, description=None):
        if self.current_user.level < 100:
            raise AAAException("The current user is not authorized to")
        if username in self._store.users:
            raise AAAException("The username is exists")
        if role not in self._store.roles:
            raise AAAException("The role is not exists")

        tstamp = str(datetime.utcnow())
        h = self._hash(username, password)
        h = h.decode('ascii')
        self._store.users[username] = {'role': role, 'hash':h, 'desc': description}
        self._store.save_users()

    def delete_user(self, username):
        if self.current_user.level < 100:
            raise AAAException("The current user is not authorized to")
        if username in self._store.users:
            raise AAAException("The username is exists")
        self.user(username).delete()

    def list_users(self):
        for username in sorted(self._store.users):
            d = self._store.users[username]
            yield (username, d['role'], d['desc'])

    @property
    def current_user(self):
        username = self.session.get('username', None)
        if username is None:
            raise AAAException("Unauthenticated user")
        if username is not None and username in self._store.users:
            return User(username, self, session=self.session)
        else:
            raise AAAException("Unknown user")

    @property
    def user_is_anoymous(self):
        try:
            username = self.session['username']
        except KeyError:
            return True

        if username not in self._store.users:
            raise AuthException("Unknown user: %s" % username)
        return False

    def user(self, username):
        if username is not None and username in self._store.users:
            return User(username, self)
        return None

    def _redirect(self, redirect_url):
        raise AAARedirect(redirect_url)

    def _setup_cookie(self, username):
        self.session['username'] = username
        if self.session_domain is not None:
            self.session.domain = self.session_domain

    def _hash(self, username, password, salt=None):
        if salt is None:
            salt = os.urandom(32)
        assert len(salt) == 32, "Incorrect salt length"

        username = username.encode('utf-8')
        password = password.encode('utf-8')

        cleartext = username + b'\0' + password
        h = hashlib.pbkdf2_hmac('sha1', cleartext, salt, 10, dklen=32)
        hashed = salt + h
        return base64.b64encode(hashed)

    def _verify_password(self, username, password, salted_hash):
        assert isinstance(salted_hash, type(b''))
        decoded = base64.b64decode(salted_hash)
        salt = decoded[:32]
        h = self._hash(username, password, salt)
        return salted_hash == h

class User():
    def __init__(self, username, auth_obj, session=None):
        self._auth = auth_obj
        assert username in self._auth._store.users, "Unknown user"
        self.username = username
        user_data = self._auth._store.users[username]
        self.role = user_data['role']
        self.description = user_data['desc']
        self.level = self._auth._store.roles[self.role]
        if session is not None:
            try:
                self.session_creation_time = session['_creation_time']
                self.session_accessed_time = session['_accessed_time']
                self.session_id = session['_id']
            except:
                pass

        def update(self, role=None, password=None):
            username = self.username
            if username not in self._auth._store.users:
                raise AAAException("User does not exist.")

            if role is not None:
                if role not in self._auth._store.rules:
                    raise AAAException("Roles does not exists")
                self._auth._store.users[username]['role'] = role

            if password is not None:
                self._auth._store.users[username]['hash'] = self._auth._hash(username,password).decode()
            self._auth._store.save_users()

        def delete(self):
            try:
                self._auth._store.users.pop(self.username)
            except KeyError:
                raise AAAException("User does not exists")
            self._auth._store.save_users()

if __name__ == "__main__":
    import sys
    auth = Authentication(directory="save", initialize=True)
