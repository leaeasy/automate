#! /usr/bin/env python
# -*- coding: utf-8 -*-
#

import os
import subprocess
import time

class CowBuilder():
    
    def __init__(self, dist, arch, configfile, logfile, buildresult):
        self.dist = dist
        self.arch = arch
        self.configfile = configfile
        self.logfile = logfile
        self.buildresult = buildresult

    def create(self, output):
        command = ["/usr/sbin/cowbuilder", "--create"]
        command.extend(["--configfile", self.configfile])

        return self.execute(command, output)

    def update(self, output):
        command = ["/usr/sbin/cowbuilder", "--update"]
        command.extend(["--configfile", self.configfile])
        if self.logfile != None:
            command.extend(["--logfile", self.logfile + ".update"])
        command.extend(["--override-config"])

        return self.execute(command, output)

    def build(self, dsc, output):
        command = ["/usr/sbin/cowbuilder", "--build", dsc]
        command.extend(["--configfile", self.configfile])
        command.extend(["--logfile", self.logfile])
        command.extend(["--buildresult", self.buildresult])
        #command.extend(["--debbuildopts", "-Zxz"])

        return self.execute(command, output)

    def execute(self, command, output):
        os.environ["DIST"] = self.dist
        os.environ["ARCH"] = self.arch
        if output:
            p = subprocess.Popen(command, shell=False)
        else:
            FNULL = open('/dev/null', 'w')
            p = subprocess.Popen(command, shell=False, stdout=FNULL, stderr=FNULL)
        retcode = p.wait()
        if not output:
           FNULL.close() 
        return retcode

