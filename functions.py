#! /usr/bin/env python2
# -*- coding: utf-8 -*-
#
import hashlib
import os
import pwd
import subprocess
import sys
import json
import socket

def debug_message(debug_active, debug_message):
    if debug_active:
        print(debug_message)

def check_root():
    usrinfo = pwd.getpwuid(os.getuid())
    if not usrinfo.pw_name == "root":
        print("E: root privileges required!")
        sys.exit(1)

def command_result(command_string, output=True):
    command = command_string.split(" ")
    if output:
        p = subprocess.Popen(command, shell=False)
    else:
        FNULL = open('/dev/null', 'w')
        p = subprocess.Popen(command, shell=False, stdout=FNULL, stderr=FNULL)
    retcode = p.wait()
    if not output:
        FNULL.close()
    return retcode

def distro_name(distro_codename):
    if distro_codename in ['unstable', 'jessie',  'testing']:
        return "debian"
    elif distro_codename in ['precise', 'trusty', 'vivid', 'wily', 'xenial', 'yakkety']:
        return "ubuntu"
    else:
        return "deepin"


def sha1file(filepath):
    if os.path.exists(filepath):
        return hashlib.sha1(open(filepath, "rb").read()).hexdigest()
    else:
        return None

def json_load(json_file):
    return json.load(open(json_file, "r"))

def json_save(json_object, json_file):
    json_fd = open(json_file, "w")
    json_fd.write(json.dumps(json_object, indent=4))
    json_fd.close()

def is_online(remoteServer, port=22):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    status = False
    try:
        result = sock.connect_ex((remoteServer, port))
        if result == 0:
            status = True
    except Exception as e: 
        print(e)
    finally:
        sock.close()
    return status
